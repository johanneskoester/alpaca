
.. highlight:: bash

.. default-role:: command

.. include:: ../README.rst

License
=======

ALPACA is available under the :doc:`MIT license </license>`.

Analysis Pipeline
=================

The pipeline used for the analysis done in the thesis can be obtained :doc:`here </analysis>`.

HTML summary of variant calls
=============================

With the show subcommand, ALPACA allows to provide a human readable summary of variant calls in a single HTML5 file.
An example can be seen :download:`here <show_example.html>`.

Author
======

.. include:: ../AUTHORS.rst

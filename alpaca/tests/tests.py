import os
import logging
import math

import numpy as np
import h5py
import pysam
import pyopencl.array as clarray

import alpaca
from alpaca.utils import init_opencl
from alpaca.index.sample_index import SampleIndex
from alpaca.index.utils import calc_genotypes, BITENCODE
from alpaca.caller import SampleUnion
from alpaca.utils import LOG_TO_PHRED_FACTOR, AlpacaError
from alpaca.ensembl import get_variant_annotation


CL_DEVICE_TYPE="cpu"
try:
    cl_queue, cl_mem_pool = init_opencl(device_type=CL_DEVICE_TYPE)
except AlpacaError:
    CL_DEVICE_TYPE="gpu"
    cl_queue, cl_mem_pool = init_opencl(device_type=CL_DEVICE_TYPE)

logging.basicConfig(
        format=" %(asctime)s: %(message)s",
        level=logging.DEBUG)


TESTDIR = os.path.dirname(__file__)
TEST_BAMS = [os.path.join(TESTDIR, "test{}.bam".format(i)) for i in range(4)]
TEST_SAMPLE_INDEX = os.path.join(TESTDIR, "{}.index.hdf5")
TEST_INDEX = os.path.join(TESTDIR, "index.hdf5")
TEST_REFERENCE = os.path.join(TESTDIR, "reference.fasta")

REFERENCE = b"AAT"
SEQS = np.fromstring(b"AAAACCCCCGGGGGG**", dtype=np.uint8)
QUALS = np.fromstring(b"=================", dtype=np.uint8)
PILEUP_ADDRESS = np.array([0, 4, 9, 17], dtype=np.int32)
PILEUP_POS = np.array([0, 1, 2], dtype=np.int32)
REF_ALLELES = np.fromstring(REFERENCE, dtype=np.uint8)
SAMPLES = np.array([0,1], dtype=np.int32)


def cleanup(path):
    if os.path.exists(path):
        os.remove(path)


def setup():

    for seq, f in zip("AA AG AA AG".split(), TEST_BAMS):
        cleanup(f)
        # first bam
        with pysam.Samfile(f, mode="wb", referencenames="A B".split(), referencelengths=[2, 2]) as samfile:
            for i in range(0):
                read = pysam.AlignedRead()
                read.pos = 0
                read.tid = 0
                read.mapq = 30
                read.seq = "AG"
                read.qual = ".."
                read.cigar = [[0, 2]]
                samfile.write(read)
            for i in range(100):
                read = pysam.AlignedRead()
                read.pos = 0
                read.tid = 0
                read.mapq = 30
                read.seq = seq
                read.qual = ".."
                read.cigar = [[0, 2]]
                samfile.write(read)
            for i in range(100):
                read = pysam.AlignedRead()
                read.pos = 0
                read.tid = 0
                read.mapq = 30
                read.seq = "AA"
                read.qual = ".."
                read.cigar = [[0, 2]]
                if i < 15:
                    read.is_reverse = True
                samfile.write(read)
        os.system("samtools index {}".format(f))

    with open(TEST_REFERENCE, "w") as fasta:
        print(">A", file=fasta)
        print("AA", file=fasta)
        print(">B", file=fasta)
        print("GG", file=fasta)
    os.system("samtools faidx {}".format(TEST_REFERENCE))


setup()


class PSEUDOINDEX:
    ploidy = 2
    genotypes = calc_genotypes(ploidy)
    genotypes_count = len(genotypes)
    sample_count = 2


def test_index_process_pileup(
    ploidy=2,
    reference=REFERENCE,
    seqs=SEQS,
    quals=QUALS,
    pileup_address=PILEUP_ADDRESS,
    pileup_pos=PILEUP_POS,
    ref_alleles=REF_ALLELES,
    test_index="process_pileup.hdf5"
):
    if os.path.exists(test_index):
        os.remove(test_index)

    with h5py.File(test_index) as hdf5:
        pileup_allelefreq_likelihoods = np.empty((3, ploidy + 1), dtype=np.float32)
        pileup_maxlikelihood_genotypes = np.empty(3, dtype=np.uint8)
        pileup_allele_depth = np.empty((3, 6), dtype=np.uint8)
        pileup_allele_rev_depth = np.empty_like(pileup_allele_depth)
        reference_positions = np.zeros(3, dtype=np.int32)

        index = SampleIndex(cl_queue, cl_mem_pool, None, ploidy=ploidy, max_pileup_depth=250)

        index.process_pileup(pileup_pos, pileup_address, ref_alleles, seqs, quals, pileup_allelefreq_likelihoods, pileup_maxlikelihood_genotypes, pileup_allele_depth, pileup_allele_rev_depth, reference_positions, 0)

        print(pileup_allelefreq_likelihoods)
        assert np.all(pileup_allelefreq_likelihoods <= 0)


def test_SampleUnion_calc_cl_pileup_probabilities():
    ploidy = 2
    genotypes = calc_genotypes(ploidy)
    pileup_allelefreq_probabilities = np.array([
        [
            [ -0.2056732,   -1.80087459, -13.9916029 ],
            [ -0.2056732,   -1.80087459, -13.9916029 ]
        ],
        [
            [-17.69043922,  -3.57210851,  -0.19433196],
            [-17.69043922,  -3.57210851,  -0.19433196]
        ],
        [
            [-24.7967701,   -4.36693335,  -0.27636975],
            [-24.7967701,   -4.36693335,  -0.27636975]
        ]
    ], dtype=np.float32)
    candidate_sites = np.array([0, 1, 2], dtype=np.int32)
    sample_union = SampleUnion(cl_queue, cl_mem_pool, PSEUDOINDEX, SAMPLES, genotypes)
    cl_pileup_probabilities = sample_union.calc_cl_pileup_probabilities(sample_union.to_device(pileup_allelefreq_probabilities), sample_union.to_device(candidate_sites))

    # TODO check the numbers here
    assert np.all(cl_pileup_probabilities <= 0)

def test_index():
    cleanup(TEST_INDEX)
    for i, bam in enumerate(TEST_BAMS):
        alpaca.index_sample(TEST_SAMPLE_INDEX.format(i), TEST_REFERENCE, [bam], alpaca.get_sample_name(bam), cl_device=CL_DEVICE_TYPE)


def test_call():
    for i, bam in enumerate(TEST_BAMS):
        alpaca.index_sample(TEST_SAMPLE_INDEX.format(i), TEST_REFERENCE, [bam], alpaca.get_sample_name(bam), cl_device=CL_DEVICE_TYPE)
    alpaca.merge(TEST_REFERENCE, TEST_INDEX, *[TEST_SAMPLE_INDEX.format(i) for i in range(4)], cl_device=CL_DEVICE_TYPE)

    query = "test3 - (test0 + test2)"
    calls = alpaca.call(TEST_INDEX, query, min_total_depth=10, max_fdr=0.05, min_qual=None, cl_device=CL_DEVICE_TYPE)


def test_variant_annotation():
    variants = [(21, 26960079, "G", "A"), (2, 15942387, "C", "G"), (1, 230845793, "A", "G"), (2, 222571887, "C", "G")]
    import json
    result = json.dumps(list(get_variant_annotation(variants, species="human")), indent=4)
    #print(result)


def calc_genotype_alt_allelefreqs(reference, genotypes):
    return np.array([[sum(1 for c in genotype if c != BITENCODE[ref]) for genotype in genotypes] for ref in reference], dtype=np.int8)


def calc_pileup_probability(pileup_allelefreq_probabilities, allelefreq_probabilities):
    sample_count = pileup_allelefreq_probabilities.shape[1]

    pileup_probability = 0
    ploidy = 2
    z = np.zeros((sample_count + 1, ploidy + 1), dtype=np.float32)
    z[0, 0] = 1
    for k in range(sample_count * ploidy + 1):
        k_ = k % (ploidy + 1)
        for j in range(1, sample_count + 1):
            s = 0
            for g in range(ploidy + 1):
                s += z[j-1,(k-g) % (ploidy + 1)] * math.exp(pileup_allelefreq_probabilities[0,j-1,g])
            z[j,k_] = s
        pileup_probability += math.exp(math.log(z[j,k_]) + allelefreq_probabilities[k])

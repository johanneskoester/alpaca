=====================================================
Analysis pipeline for ALPACA performance and accuracy
=====================================================

The following `Snakemake <http://bitbucket.org/johanneskoester/snakemake>`_ pipeline conducts the
complete performance and accuracy analysis of ALPACA as presented in my thesis
"Parallelization, Scalability and Reproducibility in Next-Generation Sequencing Analysis".
The corresponding Snakefile can be downloaded :download:`here <../analysis-pipeline/Snakefile>`.

.. literalinclude:: ../analysis-pipeline/Snakefile
   :language: python
   :tab-width: 4
